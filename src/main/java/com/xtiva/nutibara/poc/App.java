package com.xtiva.nutibara.poc;

import akka.actor.ActorSystem;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import com.xtiva.nutibara.persistence.elasticsearch.ElasticsearchRestClient;
import com.xtiva.nutibara.persistence.elasticsearch.ElasticsearchRestClientProvider;
import com.xtiva.nutibara.persistence.elasticsearch.ElasticsearchSession;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.NamedXContentRegistry;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.sql.DriverManager.getConnection;

class ColumnDefinition {
  private String columnName;
  private String type;
  public ColumnDefinition(String cn, String t) {
    this.columnName = cn;
    this.type = t;
  }
}

public class App {

  private static XContentParser indexMapping() {
    try  {
      InputStream file = ClassLoader.getSystemResourceAsStream("mappings/rep.json");
      XContentParser parser = XContentFactory.xContent(XContentType.JSON).createParser(NamedXContentRegistry.EMPTY, file);
      parser.nextToken();
      parser.nextToken();
      return parser;
    } catch (IOException e) {
      throw new UncheckedIOException("Error reading file: mappings/rep.json", e);
    }
  }

  public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException, SQLException {

    ActorSystem generalSystem = ActorSystem.create("general", ConfigFactory.load("general.conf"));
    generalSystem.log().info("Start testing elasticsearch.");

    String applicationInstanceAlias = "testtenantdev";
    ActorSystem actorSystem = ActorSystem.create(applicationInstanceAlias, ConfigFactory.load("general.conf")
            .withValue("instance.alias", ConfigValueFactory.fromAnyRef(applicationInstanceAlias))
            .withValue("elasticsearch.index", ConfigValueFactory.fromAnyRef("crigorhziw"))
            .withValue("elasticsearch-config.cluster", ConfigValueFactory.fromAnyRef(applicationInstanceAlias))
            .withValue("elasticsearch-config.host", ConfigValueFactory.fromAnyRef("10.28.13.165"))
            .withValue("elasticsearch-config.http-port", ConfigValueFactory.fromAnyRef("24593"))
            .withValue("elasticsearch-config.port", ConfigValueFactory.fromAnyRef("24594"))
            .withValue("elasticsearch-config.authentication-enabled", ConfigValueFactory.fromAnyRef(true))
            .withValue("elasticsearch-config.scheme", ConfigValueFactory.fromAnyRef("https"))
            .withValue("elasticsearch-config.ssl-enabled", ConfigValueFactory.fromAnyRef(true))
    );

    /*
    String applicationInstanceAlias = "reevaluation";
      ActorSystem actorSystem = ActorSystem.create(applicationInstanceAlias, ConfigFactory.load("general.conf")
            .withValue("instance.alias", ConfigValueFactory.fromAnyRef(applicationInstanceAlias))
            .withValue("elasticsearch.index", ConfigValueFactory.fromAnyRef("jkrnqqxdnv"))
            .withValue("elasticsearch-config.cluster", ConfigValueFactory.fromAnyRef(applicationInstanceAlias))
            .withValue("elasticsearch-config.host", ConfigValueFactory.fromAnyRef("10.66.12.11"))
            .withValue("elasticsearch-config.http-port", ConfigValueFactory.fromAnyRef("16638"))
            .withValue("elasticsearch-config.port", ConfigValueFactory.fromAnyRef("16639"))
            .withValue("elasticsearch-config.authentication-enabled", ConfigValueFactory.fromAnyRef(true))
            .withValue("elasticsearch-config.scheme", ConfigValueFactory.fromAnyRef("https"))
            .withValue("elasticsearch-config.ssl-enabled", ConfigValueFactory.fromAnyRef(true))
    );
    */

    ElasticsearchSession elasticsearchSession =  ElasticsearchSession.create(actorSystem, applicationInstanceAlias);
    ElasticsearchRestClient client = ElasticsearchRestClientProvider.service(actorSystem);

    // execute()
    IndexResponse ir  = elasticsearchSession.execute(new IndexRequest(applicationInstanceAlias + "-rep", "GET", "").source("{}", XContentType.JSON)).toCompletableFuture().get();
    generalSystem.log().info("Elasticsearch execute index response: '{}'", ir);

    // search()
    SearchRequest searchRequest = new SearchRequest(elasticsearchSession.prefixedIndex("rep"));
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    SearchResponse sr = elasticsearchSession.search(searchRequest).toCompletableFuture().get(15, TimeUnit.SECONDS);
    SearchHits hits = sr.getHits();
    generalSystem.log().info("Elasticsearch search index response: '{}'", sr);
    generalSystem.log().info("Elasticsearch search hits: '{}'", hits);

    int i=0;
    for (SearchHit hit:hits) {
      i++;
      String json = hit.getSourceAsString();
      generalSystem.log().info("Hit {}: '{}'", i, json);
    }

    // get mapping
    /*
    GetMappingsRequest getMappingsRequest = new GetMappingsRequest();
    getMappingsRequest.indices(applicationInstanceAlias + "-rep");
    GetMappingsResponse getMappingsResponse = client.admin().
    // https://www.elastic.co/guide/en/elasticsearch/client/java-rest/master/java-rest-high-get-mappings.html
    // can't find client.indices().getMapping(getMappingsRequest, RequestOptions.DEFAULT)
    */
    // XContentParser parser = indexMapping();
    // generalSystem.log().info("Mapping parser: '{}'", parser);

    /*
          "payrecord_originalNetPayout": {
            "type": "float"
          },
          "payrecord_originalPayout": {
            "type": "double"
          },
          "payrecord_paidStatus": {
            "type": "keyword",
            "fields": {
              "sortable": {
                "type": "keyword",
                "normalizer": "CaseInsensitiveSort"
              }
            }
          },
          "payrecord_payCloseDate": {
            "type": "date",
            "format": "yyyy-MM-dd"
          },
          "payrecord_payRecordStatus": {
            "type": "keyword",
            "fields": {
              "sortable": {
                "type": "keyword",
                "normalizer": "CaseInsensitiveSort"
              }
            }
          },
          "payrecord_payee": {
            "properties": {
              "glNumber": {
                "type": "keyword",
                "fields": {
                  "searchable": {
                    "type": "text",
                    "analyzer": "BasicSearchableAnalyzer"
                  },
                  "sortable": {
                    "type": "keyword",
                    "normalizer": "CaseInsensitiveSort"
                  }
                }
              },
              "id": {
                "type": "keyword",
                "fields": {
                  "sortable": {
                    "type": "keyword",
                    "normalizer": "CaseInsensitiveSort"
                  }
                }
              },
              "payeeEid": {
                "type": "keyword",
                "fields": {
                  "searchable": {
                    "type": "text",
                    "analyzer": "BasicSearchableAnalyzer"
                  },
                  "sortable": {
                    "type": "keyword",
                    "normalizer": "CaseInsensitiveSort"
                  }
                }
              },
              "payeeName": {
                "type": "keyword",
                "fields": {
                  "searchable": {
                    "type": "text",
                    "analyzer": "BasicSearchableAnalyzer"
                  },
                  "sortable": {
                    "type": "keyword",
                    "normalizer": "CaseInsensitiveSort"
                  }
                }
              },
              "sourceNodeType": {
                "type": "keyword",
                "fields": {
                  "searchable": {
                    "type": "text",
                    "analyzer": "BasicSearchableAnalyzer"
                  },
                  "sortable": {
                    "type": "keyword",
                    "normalizer": "CaseInsensitiveSort"
                  }
                }
              }
            }
          },
     */
    String table_name = "payrecord";
    ArrayList<ColumnDefinition> columns = new ArrayList();
    JSONParser parser = new JSONParser();
    try {
      Object obj = parser.parse(new FileReader("/home/ubuntu/git/xtiva/poc-elasticsearch-to-classic/src/main/resources/mappings/payrecord.json"));
      JSONObject jsonObject = (JSONObject) obj;
      JSONObject reevaluation_payrecord_v6 = (JSONObject) jsonObject.get("reevaluation-payrecord-v6");
      JSONObject mappings = (JSONObject) reevaluation_payrecord_v6.get("mappings");
      JSONObject payrecord = (JSONObject) mappings.get("payrecord");
      JSONObject properties = (JSONObject) payrecord.get("properties");
      for (Object o: properties.keySet()) {
        ColumnDefinition cd = new ColumnDefinition(o.toString(), "varchar");
        columns.add(cd);
        System.out.println(o);
      }
      System.out.print(columns);
    } catch (Exception e) {
      e.printStackTrace();
    }

    String url = "jdbc:h2:~/test";
    Connection conn = DriverManager.getConnection(url, "sa", "");
    Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery("SELECT 1+1");
    generalSystem.log().info("Result: '{}'", rs);
    conn.close();
  }

}
