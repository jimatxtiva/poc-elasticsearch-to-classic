package com.xtiva.nutibara.query;

import akka.actor.ActorSystem;
import akka.event.LoggingAdapter;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import com.xtiva.nutibara.persistence.elasticsearch.ElasticsearchSession;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.NamedXContentRegistry;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class ColumnDefinition {
  private String columnName;
  private String type;
  public ColumnDefinition(String cn, String t) {
    this.columnName = cn;
    this.type = t;
  }
}

public class App {

  private static XContentParser indexMapping() {
    try  {
      InputStream file = ClassLoader.getSystemResourceAsStream("mappings/payrecord.json");
      XContentParser parser = XContentFactory.xContent(XContentType.JSON).createParser(NamedXContentRegistry.EMPTY, file);
      parser.nextToken();
      parser.nextToken();
      return parser;
    } catch (IOException e) {
      throw new UncheckedIOException("Error reading file: mappings/payrecord.json", e);
    }
  }

  private List<String[]> getIndices() throws IOException {
    // String filename = String.valueOf(this.getClass().getResource("/indices.csv"));
    String filename = "/home/ubuntu/git/xtiva/poc-elasticsearch-to-classic.old/src/main/resources/indices.csv";
    BufferedReader csvReader = new BufferedReader(new FileReader(filename));
    String row;
    List<String[]> indices = new ArrayList<>();
    while ((row = csvReader.readLine()) != null) {
      String[] data = row.split(",");
      indices.add(data);
    }
    return indices;
  }

  private static String getColumnType(String type, String format) {
    String columnType = "";
    if (type.equals("keyword")) {
      columnType = "VARCHAR(255)";
    } else if (type.equals("date")) {
      columnType = "DATE";
      if (format != null && format.equals("yyyy-MM-dd HH:mm:ss.SSS")) {
        columnType = "DATETIME";
      }
    } else if (type.equals("boolean")) {
      columnType = "BOOLEAN";
    } else if (type.equals("object") || type.equals("text")) {
      columnType = "TEXT";
    } else if (type.equals("integer")) {
      columnType = "INTEGER";
    } else if (type.equals("long")) {
      columnType = "BIGINT";
    } else if (type.equals("double")) {
      columnType = "DOUBLE";
    }
    return columnType;
  }

  private void dumpData(ElasticsearchSession elasticsearchSession, Connection conn, LoggingAdapter log, String indexName, String indexType, String tableName) throws InterruptedException, ExecutionException, TimeoutException, SQLException {
    // search()
    /*
    SearchRequest searchRequest = new SearchRequest(elasticsearchSession.prefixedIndex("rep"));
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    SearchResponse sr = elasticsearchSession.search(searchRequest).toCompletableFuture().get(15, TimeUnit.SECONDS);
    */
    SearchResponse sr = elasticsearchSession.search(QueryBuilders.matchAllQuery(), indexName, indexType).toCompletableFuture().get(60, TimeUnit.SECONDS);
    SearchHits hits = sr.getHits();
    log.info("Elasticsearch search index response: '{}'", sr);
    log.info("Elasticsearch search hits: '{}'", hits);

    Map<String, Object> mapping = elasticsearchSession.mapping(indexName, Optional.ofNullable(indexType)).get();
    Map<String, Object> index = (Map<String, Object>) (mapping.get(indexName));
    Map<String, Object> mappings = (Map<String, Object>) (index.get("mappings"));
    Map<String, Object> rep = (Map<String, Object>) (mappings.get(indexType));
    Map<String, Object> properties = (Map<String, Object>) (rep.get("properties"));
    // System.out.println(properties);
    Map<String, String> columnMap = new HashMap<>();

    String sql = "CREATE TABLE " + tableName + " (";
    int i = 0;
    for (String key : properties.keySet()) {
      Map<String, Object> keyMap = (Map<String, Object>) (properties.get(key));
      String type = "";
      String format = null;
      if (keyMap.keySet().contains("type")) {
        type = (String) keyMap.get("type");
        format = (String) keyMap.get("format");
        if (i > 0) {
          sql += ", ";
        }
        i++;
        String columnName = key;
        String columnType = getColumnType(type, format);
        columnMap.put(columnName, columnType);
        sql += "`" + columnName + "` " + columnType;
        // System.out.println(key + " " + type);
      } else if (keyMap.keySet().contains("properties")) {
        Map<String, Object> columnProperties = (Map<String, Object>) (keyMap.get("properties"));
        for (String columnKey: columnProperties.keySet()) {
          type = "";
          Map<String, Object> columnKeyMap = (Map<String, Object>) (columnProperties.get(columnKey));
          if (columnKeyMap.keySet().contains("type")) {
            type = (String) columnKeyMap.get("type");
            format = (String) columnKeyMap.get("format");
            if (i > 0) {
              sql += ", ";
            }
            i++;
            String columnName = key + "." + columnKey;
            String columnType = getColumnType(type, format);
            columnMap.put(columnName, columnType);
            sql += "`" + columnName + "` " + columnType;
            // System.out.println(columnKey + " " + type);
          }
        }
      }
    }
    sql += ");";
    log.info(sql);

    Statement stmt = conn.createStatement();
    Boolean dropped = stmt.execute("DROP TABLE " + tableName + " IF EXISTS;");
    if (dropped) {
      log.info("Table existed, dropped table.");
    }
    Boolean executed = stmt.execute(sql);
    if (executed) {
      log.info("Finished creating table.");
    }

    i = 0;

    for (SearchHit hit : hits) {
      List<String> columns = new ArrayList();
      String sqlInsert = "";
      i++;
      String jsonStr = hit.getSourceAsString();
      try {
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(jsonStr);
        int c = 0;
        for(Iterator iterator = json.keySet().iterator(); iterator.hasNext();) {
          String columnName = (String) iterator.next();

          String value = "";
          if (json.get(columnName)!=null) {
            value = json.get(columnName).toString();
          }

          String columnType = columnMap.get(columnName);
          if (columnType == null) {
            // System.out.println(columnName);
            // System.out.println(value);
            JSONObject subJson;
            try {
              subJson = (JSONObject) parser.parse(value);
            } catch (Exception e) {
              System.out.println(columnName);
              e.printStackTrace();
              continue;
            }
            // System.out.println(subJson.toString());
            for (Iterator iteratorSub = subJson.keySet().iterator(); iteratorSub.hasNext();) {
              String columnNameSub = (String)iteratorSub.next();
              String columnNameCombine = columnName + "." + columnNameSub;
              value = "";
              if (subJson.get(columnNameSub)!=null) {
                value = subJson.get(columnNameSub).toString();
              }
              columnType = columnMap.get(columnNameCombine);
              System.out.println("Processing " + columnNameCombine);
              if (columnType==null) {
                // This case should not happen. It means while inspecting index _mapping, there is something wrong
                // An example is: testtenantdev-compensation-v7.compensation_security.securityType
                //    Current program expect above securityType is a field but it is a object that contains deeper fields. While parsing _mapping, this securityType is not properly recorded.
                //    Therefore in this step, columnType is null.
                continue;
              }
              columns.add("`" + columnNameCombine + "`");
              if (c>0) {
                sqlInsert += ", ";
              }
              c++;
              if (columnType.equals("BOOLEAN")) {
                if (value.equals("ACTIVE")) {
                  value = "true";
                } else {
                  value = "false";
                }
                sqlInsert += value;
              } else if ((columnType.equals("DATE") || columnType.equals("DATETIME") || columnType.equals("INTEGER") || columnType.equals("DOUBLE") || columnType.equals("BIGINT")) && value.equals("")) {
                value = "null";
                sqlInsert += value;
              } else {
                sqlInsert += "'" + value + "'";
              }
              System.out.println(columnNameCombine + " " + sqlInsert);
            }
          } else {
            columns.add("`" + columnName + "`");
            if (c>0) {
              sqlInsert += ", ";
            }
            c++;
            if (columnType.equals("BOOLEAN")) {
              if (value.equals("ACTIVE")) {
                value = "true";
              } else {
                value = "false";
              }
              sqlInsert += value;
            } else if ((columnType.equals("DATE") || columnType.equals("DATETIME") || columnType.equals("INTEGER") || columnType.equals("DOUBLE") || columnType.equals("BIGINT")) && value.equals("")) {
              value = "null";
              sqlInsert += value;
            } else {
              sqlInsert += "'" + value + "'";
            }
            System.out.println(columnName + " " + sqlInsert);
          }
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }
      log.info("Hit {}: '{}'", i, jsonStr);
      sqlInsert = "INSERT INTO " + tableName + "(" + String.join(",", columns) + ") VALUES (" + sqlInsert + ");";
      log.info(sqlInsert);
      try {
        executed = stmt.execute(sqlInsert);
        if (executed) {
          log.info(sqlInsert);
        }
      } catch(Exception e) {
        e.printStackTrace();
        continue;
      }
    }

  }

  public static void main(String[] args) throws IOException, SQLException, InterruptedException, ExecutionException, TimeoutException {
    App app = new App();

    // read indices configuration from CVS file
    List<String[]> indices = app.getIndices();

    // create a general actor
    ActorSystem generalSystem = ActorSystem.create("general", ConfigFactory.load("general.conf"));

    LoggingAdapter log = generalSystem.log();
    log.info("Start testing elasticsearch.");

    String applicationInstanceAlias = "testtenantdev";

    // create actor system
    ActorSystem actorSystem = ActorSystem.create(applicationInstanceAlias, ConfigFactory.load("general.conf")
            .withValue("instance.alias", ConfigValueFactory.fromAnyRef(applicationInstanceAlias))
            .withValue("elasticsearch.index", ConfigValueFactory.fromAnyRef("crigorhziw"))
            .withValue("elasticsearch-config.cluster", ConfigValueFactory.fromAnyRef(applicationInstanceAlias))
            .withValue("elasticsearch-config.host", ConfigValueFactory.fromAnyRef("10.28.13.103"))
            .withValue("elasticsearch-config.http-port", ConfigValueFactory.fromAnyRef("13991"))
            .withValue("elasticsearch-config.port", ConfigValueFactory.fromAnyRef("13992"))
            .withValue("elasticsearch-config.authentication-enabled", ConfigValueFactory.fromAnyRef(true))
            .withValue("elasticsearch-config.scheme", ConfigValueFactory.fromAnyRef("https"))
            .withValue("elasticsearch-config.ssl-enabled", ConfigValueFactory.fromAnyRef(true))
    );

    ElasticsearchSession elasticsearchSession = ElasticsearchSession.create(actorSystem, applicationInstanceAlias);

    // execute() example
    IndexResponse ir = elasticsearchSession.execute(new IndexRequest(applicationInstanceAlias + "-rep", "GET", "").source("{}", XContentType.JSON)).toCompletableFuture().get();
    generalSystem.log().info("Elasticsearch execute index response: '{}'", ir);

    // connect to H2 database
    String url = "jdbc:h2:~/test";
    Connection conn = DriverManager.getConnection(url, "sa", "");

    for (String[] index: indices) {
      log.info("Working on " + index[0] + ", " + index[1] + ", " + index[2]);
      app.dumpData(elasticsearchSession, conn, log, index[0], index[1], index[2]);
    }

    // close H2 connection
    conn.close();

    // shutdown
    generalSystem.terminate();
    actorSystem.terminate();
  }

}
